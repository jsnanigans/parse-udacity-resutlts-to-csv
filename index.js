const path = require('path');
const ObjectsToCsv = require('objects-to-csv');

const fs = require('fs');

function readFiles(dirname, onFileContent, onError) {
  const all = [];
  return new Promise(resolve => {
    fs.readdir(dirname, function(err, filenames) {
      if (err) {
        onError(err);
        return;
      }
      filenames.forEach(function(filename, index) {
        fs.readFile(dirname + '/' + filename, 'utf-8', function(err, content) {
          if (err) {
            onError(err);
            return;
          }
          all.push(onFileContent(filename, content));
          if (all.length === filenames.length) {
            resolve(all);
          }
        });
      });
    });
  });
}

const files = path.resolve('./testresults/');

const getGroups = (regex, content) => {
  let match = regex.exec(content);
  const groups = [];
  while (match) {
    match.forEach((e, i) => {
      if (i > 0) {
        groups.push(e);
      }
    });
    match = regex.exec(content);
  }

  return groups;
};

const _name = /using\ ([a-z_]+)/gm;
const _heuristic = /with\ (\w+)/gm;
const _length = /length\:\ (\d+)/gm;
const _time = /seconds\:\ ([\d\.]+)$/gm;
const _table = /^\ +(\d+)\ +(\d+)\ +(\d+)\ +(\d+)/gm;

const lookupName = name => {
  const map = {
    astar_search: 'A*',
    breadth_first_search: 'BFS',
    depth_first_graph_search: 'DFS',
    greedy_best_first_graph_search: 'Greedy Best First',
    uniform_cost_search: 'UCS'
  };

  return map[name] || '';
};

const lookupH = name => {
  const map = {
    h_pg_levelsum: 'Levelsum',
    h_pg_maxlevel: 'Maxlevel',
    h_pg_setlevel: 'Setelvel',
    h_unmet_goals: 'Unmet Goals'
  };

  return map[name] ? `, H: ${map[name]}` : '';
};

const handleFile = (file, content) => {
  let data = {
    test: file.split('.')[0]
  };
  try {
    const name = getGroups(_name, content);
    const table = getGroups(_table, content);
    const time = getGroups(_time, content);
    const length = getGroups(_length, content);
    const heuristic = getGroups(_heuristic, content);

    data.searchFn = `${lookupName(name[0])}${lookupH(heuristic[0])}`;
    data.actions = parseInt(table[0], 10) || false;
    data.expansions = parseInt(table[1], 10) || false;
    data.goalTest = parseInt(table[2], 10) || false;
    data.newNodes = parseInt(table[3], 10) || false;
    data.seconds = parseFloat(time[0], 10) || false;
    data.planLength = parseInt(length[0], 10) || false;
  } catch (error) {
    console.log(error);
  }

  return data;
};

(async () => {
  const data = await readFiles(files, handleFile, e => console.log(e));

  const csv = new ObjectsToCsv(data);

  // Save to file:
  await csv.toDisk('./results.csv');

  // Return the CSV file as string:
  console.log(await csv.toString());
})();
